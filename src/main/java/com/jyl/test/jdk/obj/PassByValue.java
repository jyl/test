/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.jdk.obj;

/**
 * @TODO
 * @author Long
 * @date 2018年8月27日下午6:49:53
 */
public class PassByValue {
	
	public static void main(String[] args) {
		
		Dog dog = new Dog("A");
	
		System.out.println(dog); //Dog@15db9742
		
		func(dog);
		
		System.out.println(dog); //Dog@15db9742
		System.out.println(dog.getName()); //A
		
	}
	
	public static void func(Dog dog){
		
		System.out.println(dog); //Dog@15db9742
		
		dog = new Dog("B");
		
		System.out.println(dog); //Dog@6d06d69c
		System.out.println(dog.getName()); //B
		
	}
	
	/*public static void main(String[] args) {
        Dog dog = new Dog("A");
        func(dog);
        System.out.println(dog.getName()); // B
    }

    private static void func(Dog dog) {
        dog.setName("B");
    }*/
}
