/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.jdk.obj;

/**
 * @TODO
 * @author Long
 * @date 2018年8月27日下午6:50:11
 */
public class Dog {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Dog(String name){
		this.name = name;
	}
}
