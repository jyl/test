/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.jdk.io;

import java.io.File;

/**
 * @TODO
 * @author Long
 * @date 2018年9月12日上午11:27:35
 */
public class ListAllFile {
	
	public static void main(String[] args) {
	
		File file = new File("C:\\Users\\Long\\Desktop\\集市");
		ListAllFile.listAllFile(file);
	}
	
	public static void listAllFile(File dir){
		
		if(dir == null || !dir.exists()){
			return;
		}
		
		if(dir.isFile()){
			System.out.println(dir.getName());
			return;
		}
		
		for(File file : dir.listFiles()){
			listAllFile(file);
		}
	}

}
