package com.jyl.test.util;

public class TestIp2Long {

	private static long ip2Long(String strIp) 
	//将127.0.0.1 形式的ip地址转换成10进制整数，这里没有进行任何错误处理 
	{ 
		long [] ip=new long[4]; 
		int position1=strIp.indexOf("."); 
		int position2=strIp.indexOf(".",position1+1); 
		int position3=strIp.indexOf(".",position2+1); 
		
		ip[0]=Long.parseLong(strIp.substring(0,position1)); 
		ip[1]=Long.parseLong(strIp.substring(position1+1,position2)); 
		ip[2]=Long.parseLong(strIp.substring(position2+1,position3)); 
		ip[3]=Long.parseLong(strIp.substring(position3+1)); 
		return (ip[0]<<24)+(ip[1]<<16)+(ip[2]<<8)+ip[3]; //ip1*256*256*256+ip2*256*256+ip3*256+ip4 
	} 
	
	public static void main(String[] args) {
		System.out.println(ip2Long("1.184.118.156"));
	}
    
    /*public static void main(String[] args) {
    	List<Object[]> list = new ArrayList<Object[]>();
		list.add(new Object[]{"100","P"});
		list.add(new Object[]{"100","S"});
		list.add(new Object[]{"101","P"});
		list.add(new Object[]{"101","P"});
		list.add(new Object[]{"100","P"});
		
		Map<String,Integer> map = new HashMap<String,Integer>();
		for(Object[] obj : list){
			String key = (String)obj[0]+","+(String)obj[1];
			if(map.containsKey(key)){
				map.put(key, map.get(key)+1);
			}else{
				map.put(key, 1);
			}
		}
		System.out.println(map);
		
		Map<String,String> countMap = new HashMap<String,String>();
		for(String str : map.keySet()){
			String key = str.split(",")[0];
			String count = str.split(",")[1];
			if(countMap.containsKey(key)){
				countMap.put(key,countMap.get(key)+","+count+map.get(str));
			}else{
				countMap.put(key,count+map.get(str));
			}
		}
		System.out.println(countMap);
	}*/
}