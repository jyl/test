/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.jvm.test;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 * VM args: -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError -XX:+HeapDumpOnOutOfMemoryError
 * @author Long
 * @date 2018年8月27日下午7:47:52
 */
public class HeapOOM {
	
	static class OOMObject{
		
	}
	
	public static void main(String[] args) {
		
		List<OOMObject> list = new ArrayList<OOMObject>();
		
		while(true){
			list.add(new OOMObject());
		}
	}

}
