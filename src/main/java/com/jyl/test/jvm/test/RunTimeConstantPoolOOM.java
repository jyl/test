/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.jvm.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @TODO -XX:PermSize=10M -XX:MaxPermSize=10M
 * @author Long
 * @date 2018年9月4日下午7:49:49
 */
public class RunTimeConstantPoolOOM {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();
		
		int i = 0;
		
		while(true){
			list.add(String.valueOf(i++).intern());
		}
	}
}
