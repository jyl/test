package com.jyl.test.jvm;

/**
 * @TODO
 * @author Long
 * @date 2018年8月13日下午4:23:23
 */
public class VolatileTest {
	
	private static volatile VolatileTest instance;
	
	public static VolatileTest getInstance(){
		if(instance == null){
			instance = new VolatileTest();
		}
		return instance;
	}

	public static void main(String[] args) {
		
		//Lock putstatic
		VolatileTest.getInstance();
	}
}
