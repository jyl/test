package com.jyl.test.jvm;

/**
 * @TODO
 * @author Long
 * @date 2018年8月10日上午10:42:35
 */
public class ReferenceCountingGC {

	public Object instance = null;

    public static void main(String[] args) {
        ReferenceCountingGC objectA = new ReferenceCountingGC();
        ReferenceCountingGC objectB = new ReferenceCountingGC();
        objectA.instance = objectB;
        objectB.instance = objectA;
    }
}
