package com.jyl.test.jvm;

import java.util.HashSet;
import java.util.Set;

import com.jyl.test.jdk.genericity.User;

/**
 * @TODO
 * @author Long
 * @date 2018年7月17日下午3:10:29
 */
public class Test {
	
	//内存泄露
	public static void main(String[] args) {
		
		Set<User> set = new HashSet<User>();
		
		for(int i=0;i<100000;i++){
			User obj = new User();
			set.add(obj);
			
			// 设置为空，这对象我不再用了
			obj = null;
		}
		
		// 但是set集合中还维护这obj的引用，gc不会回收object对象      
        System.out.println(set);
	}

}
