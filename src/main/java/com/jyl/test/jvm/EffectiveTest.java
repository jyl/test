package com.jyl.test.jvm;

/**
 * @TODO 由于多线程存在脏读，导致最终count<=1000
 * @author Long
 * @date 2018年8月13日下午2:25:43
 */
public class EffectiveTest {
	
	public static int count = 0;
	
	public synchronized static void add(){
		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		count++;
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		for(int i=0;i<1000;i++){
			new Thread(EffectiveTest::add).start();
		}
		
		Thread.sleep(5000);
		
		System.out.println(count);
	}

}
