/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.sort;

import java.util.Arrays;

/**
 * @TODO
 * @author Long
 * @date 2019年5月5日上午10:17:22
 */
public class BubbleSort {
	
	public static void main(String[] args) {
		
		int[] arr = {3,6,5,8,1,2,9,7,4};
		
		System.out.println(Arrays.toString(arr));
		for(int i=arr.length;i>1;i--){
			System.out.println("----------"+i);
			for(int j=0;j<i-1;j++){
				System.out.println("========="+j);
				int first = arr[j];
				int last = arr[j+1];
				if(first>last){
					arr[j] = last;
					arr[j+1] = first;
				}
			}
		}
		System.out.println(Arrays.toString(arr));
		
	}

}
