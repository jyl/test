/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.tree.btree;

/**
 * @TODO
 * @author Long
 * @date 2019年4月24日上午10:05:14
 */
public class TreeNode {

	private String val;
	private TreeNode lNode;
	private TreeNode rNode;
	
	public TreeNode(String val){
		this.val = val;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public TreeNode getlNode() {
		return lNode;
	}

	public void setlNode(TreeNode lNode) {
		this.lNode = lNode;
	}

	public TreeNode getrNode() {
		return rNode;
	}

	public void setrNode(TreeNode rNode) {
		this.rNode = rNode;
	}

}
