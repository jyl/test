/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.tree.btree;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @TODO
 * @author Long
 * @date 2019年4月24日上午10:06:27
 */
public class BTreeTest {
	
	/**
	 * 构造数据
	 * @return
	 */
	public static TreeNode constructData(){
		
		TreeNode node1 = new TreeNode("1");
		TreeNode node2 = new TreeNode("2");
		TreeNode node3 = new TreeNode("3");
		TreeNode node4 = new TreeNode("4");
		TreeNode node5 = new TreeNode("5");
		
		node1.setlNode(node2);
		node2.setlNode(node3);
		node2.setrNode(node4);
		node1.setrNode(node5);
		
		return node1;
	}
	
	/**
	 * 遍历数据
	 * @param queue
	 */
	public static ArrayList<String> getNodes(TreeNode treeNode){
		
		ArrayList<String> resultArray = new ArrayList<String>();
		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(treeNode);
		
		TreeNode tmp;
		while(!queue.isEmpty()){
			tmp = queue.poll();
			
			if(tmp.getlNode() != null){
				queue.add(tmp.getlNode());
			}
			
			if(tmp.getrNode() != null){
				queue.add(tmp.getrNode());
			}
			resultArray.add(tmp.getVal());
		}
		return resultArray;
	}
	
	public static void main(String[] args) {
		ArrayList<String> resultArray = BTreeTest.getNodes(BTreeTest.constructData());
		for(String str : resultArray){
			System.out.println(str);
		}
	}

}
