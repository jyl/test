/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.tree;

/**
 * @TODO
 * @author Long
 * @date 2019年4月24日上午9:50:41
 */
public class TreeNode {
	
	public String val;
    public TreeNode leftTree = null;
    public TreeNode rightTree = null;

    public TreeNode(String val) {
        this.val = val;
    }
}
