/**
 * Copyright (C) 2018 Long Inc., All Rights Reserved.
 */
package com.jyl.test.tree;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @TODO
 * @author Long
 * @date 2019年4月24日上午9:49:23
 */
public class BTree {
	
    public static void main(String[] args) {
        // 队列
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        
        // 模拟数据
        TreeNode root = new TreeNode("1");
        TreeNode root2 = new TreeNode("2");
        TreeNode root3 = new TreeNode("3");
        TreeNode root4 = new TreeNode("4");
        TreeNode root5 = new TreeNode("5");
        root.leftTree = root2;
        root.rightTree = root5;
        root.leftTree.leftTree = root3;
        root.leftTree.rightTree = root4;
        
        // 将头节点加入队列
        queue.add(root);
        
        TreeNode temp = null;
        
        // 收集结果
        ArrayList<String> resultArray = new ArrayList<String>();
        
        // 通过while循环，将队列内容全部取出
        while (!queue.isEmpty()) {
            // 取出队列第一个节点
            temp = queue.poll();
            
            // 该节点若有左子树，则添加至队列尾部
            if (temp.leftTree != null) {
                queue.add(temp.leftTree);
            }
            
            // 该节点若有右子树，则添加至队列尾部
            if (temp.rightTree != null) {
                queue.add(temp.rightTree);
            }
            // 保存结果
            resultArray.add(temp.val);
        }
        
        // 输出结果
        for (String str : resultArray) {
            System.out.println(str);
        }
    }

}
